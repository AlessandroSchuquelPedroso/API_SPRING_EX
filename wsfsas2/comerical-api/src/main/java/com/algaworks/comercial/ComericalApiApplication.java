package com.algaworks.comercial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComericalApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComericalApiApplication.class, args);
	}

}
